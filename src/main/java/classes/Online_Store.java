/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;
import classes.Product;

import java.util.ArrayList;

/**
 *
 * @author jcebalus
 */
public class Online_Store {
    
    ArrayList<Product> products = new ArrayList<Product>();
    
    
    public void register_product(int code, String product_name, float price, int amounts){
        products.add(new Product(code, product_name, price, amounts));
    }
    
    public String show_products(){
        String output  = "PRODUCTOS:\n\n";
        for(int i = 0; i < products.size(); i++){
            output += "Código: "+ products.get(i).getCode();
            output += "\nProducto: "+ products.get(i).getProduct_name();
            output += "\nPrecio: $"+ products.get(i).getPrice();
            output += "\nCantidad: "+ products.get(i).getAmounts();
            output += "\n"+"\n";
        }
        return output;
    }
    
    public String check_storage(){
        String output = "Próximo a agotarse:\n\n";
        for (int i = 0; i < products.size(); i++){
            if(products.get(i).getAmounts()< 3){
                output += "\nProducto: "+products.get(i).getProduct_name();
            }
        }
        return output;
    } 

    @Override
    public String toString() {
        String output = "DETALLES\n\n";
        for(int i = 0; i < products.size(); i++){
            if(products.get(i).getAmounts()< 3){
                output += "Código: "+ products.get(i).getCode();
                output += "\nProducto: "+ products.get(i).getProduct_name();
                output += "\nPrecio: $"+ products.get(i).getPrice();
                output += "\nCantidad: "+ products.get(i).getAmounts();
                output += "\n"+"\n";
            }
        }
        return output;
    }
    
    
}
