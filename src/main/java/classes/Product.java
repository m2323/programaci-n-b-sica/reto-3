/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author jcebalus
 */
public class Product {
    private int code;
    private String product_name;
    private float price;
    private int amounts;

    public Product(int code, String product_name, float price, int amounts) {
        this.code = code;
        this.product_name = product_name;
        this.price = price;
        this.amounts = amounts;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getAmounts() {
        return amounts;
    }

    public void setAmounts(int amounts) {
        this.amounts = amounts;
    }

    @Override
    public String toString() {
        return "Code: " + code + "\tProduct: " + product_name + "/tPrice: " + price + "\tAmounts: " + amounts;
    }
    
    
}
