/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reto_3;
import classes.Online_Store;
import javax.swing.JOptionPane;
/**
 *
 * @author jcebalus
 */
public class Main {
    public static void main(String[] args) {
        String opc;
        String output;
        
        Online_Store store = new Online_Store();
        
        do {
            opc = JOptionPane.showInputDialog(null,"\n1. Ingresar Productos"
                                              +"\n2. Mostrar Productos"
                                              +"\n3. Revisar Inventario"
                                              +"\n0. Salir");
            
            switch(opc){
                case "1":
                    int code = 0;
                    String product_name;
                    float price = 0;
                    int amounts = 0;
                    int products = 0;
                    boolean check = false;
                    
                    do{
                        try {
                            products = Integer.parseInt(JOptionPane.showInputDialog(null, "Cantidad de productos a Ingresar: "));
                            check = true;
                        } catch (Exception e) {
                            JOptionPane.showInternalMessageDialog(null, "Tipo de dato ingresado es erroneo.");
                        }
                    }while(check == false);
                    check = false;
                    for (int i = 0; i < products; i++){
                        String title = "Producto "+(i+1)+"\n\n";
                        do{
                            try {
                                code = Integer.parseInt(JOptionPane.showInputDialog(null, title+"Código: "));
                                check = true;
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "El código debe ser un número entero.");
                            }
                        }while(check==false);
                        check = false;
                        product_name = JOptionPane.showInputDialog(null, title+"Nombre: ");
                        do {                            
                            try {
                                price = Float.parseFloat(JOptionPane.showInputDialog(null, title+"Precio: "));
                                check = true;
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "El precio debe ser un número.");
                            }
                        } while (check==false);
                        check=false;
                        do {                            
                            try {
                                amounts = Integer.parseInt(JOptionPane.showInputDialog(null, title+"Cantidad: "));
                                check=true;
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "La cantidad debe ser un número entero.");
                            }
                        } while (check==false);
                        store.register_product(code, product_name, price, amounts);
                    }                    
                    break;
                case "2":
                    output = store.show_products();
                    JOptionPane.showMessageDialog(null, output);
                    break;
                case "3":
                    output = store.check_storage();
                    JOptionPane.showMessageDialog(null, output);
                    output = store.toString();
                    JOptionPane.showMessageDialog(null, output);
                    break;
                case "0":
                    System.exit(0);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion invalida !");
                    break;
            }
            
        }while(!opc.equals("0"));
    }
}
